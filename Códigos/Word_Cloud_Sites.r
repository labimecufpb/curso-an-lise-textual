#############################################################
###  Algoritmo para criação de word cloud direto do site ####
#############################################################


# Instalação Packages #

install.packages("tcltk")
install.packages("tmap")
install.packages("tm")
install.packages("SnowballC")
install.packages("RColorBrewer")
install.packages("wordcloud")
install.packages("wordcloud2")

# Carregando pacote
library(tcltk) 
library(tmap)
library(tm)
library(SnowballC)
library(RColorBrewer)
library(wordcloud)
library(wordcloud2)

####importando a função

# a url pode ser substituida conforme a necessidade do pesquisador.


source('http://www.sthda.com/upload/rquery_wordcloud.r')
url = "https://www.folha.uol.com.br/"
a<- rquery.wordcloud(x=url, type= "url", lang = "portuguese", excludeWords = NULL, textStemming = FALSE,  colorPalette="Dark2")

wordcloud2 (a$freqTable)